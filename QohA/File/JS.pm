package QohA::File::JS;

use Modern::Perl;
use Moo;

extends 'QohA::File';

sub run_checks {
    my ($self, $cnt) = @_;

    my $r;

    $r = $self->check_tidiness;
    $self->SUPER::add_to_report( 'tidiness', $r );

    if ( $self->pass == 0 ) {
        $self->SUPER::add_to_report('forbidden patterns', '');
    } else {
        $r = $self->check_forbidden_patterns($cnt);
        $self->SUPER::add_to_report('forbidden patterns', $r);
    }

    return $self->SUPER::run_checks($cnt);
}

sub check_forbidden_patterns {
    my ($self, $cnt) = @_;

    my @forbidden_patterns = (
        {pattern => qr{console.log} , error => "console.log"},
        {pattern => qr{^<<<<<<<}, error => "merge marker (<<<<<<<)"},# git merge non terminated
        {pattern => qr{^>>>>>>>}, error => "merge marker (>>>>>>>)"},
        {pattern => qr{^=======}, error => "merge marker (=======)"},
        {pattern => qr{\.only\(}, error => ".only detected"},
        {pattern => qr{[^_]_\(}, error => 'Use __("") instead of _("")'},
    );

    my $errors = $self->SUPER::check_forbidden_patterns($cnt, \@forbidden_patterns);
    return q{} if $errors == 1;
    return $errors;
}

sub check_tidiness {
    my ($self) = @_;

    return 1 unless -f '.prettierrc.js'; # We run from tests, skip: we don't have access to the prettier config and plugins
    return $self->SUPER::check_tidiness
}

1;

=head1 AUTHOR

Jonathan Druart <jonathan.druart@bugs.koha-community.org>

=head1 COPYRIGHT AND LICENSE

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut
