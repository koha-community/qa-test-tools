package QohA::File::Invalid;

use Modern::Perl;
use Moo;

extends 'QohA::File';

sub run_checks {
    my ($self, $cnt) = @_;

    $self->SUPER::add_to_report('invalid file', ($self->pass == 0 ? 0 : ["This file must not be added to the codebase"]));

    return $self->SUPER::run_checks($cnt);
}

1;

=head1 AUTHOR

Jonathan Druart <jonathan.druart@bugs.koha-community.org>

=head1 COPYRIGHT AND LICENSE

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut
