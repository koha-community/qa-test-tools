package QohA::File::Vue;

use Modern::Perl;
use Moo;

extends 'QohA::File';

sub run_checks {
    my ($self, $cnt) = @_;

    my $r = $self->check_tidiness;
    $self->SUPER::add_to_report('tidiness', $r);

    return $self->SUPER::run_checks($cnt);
}

sub check_tidiness {
    my ($self) = @_;

    return 1 unless -f '.prettierrc.js'; # We run from tests, skip: we don't have access to the prettier config and plugins
    return $self->SUPER::check_tidiness
}

1;

=head1 AUTHOR

Jonathan Druart <jonathan.druart@bugs.koha-community.org>

=head1 COPYRIGHT AND LICENSE

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut
